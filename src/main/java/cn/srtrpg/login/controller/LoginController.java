package cn.srtrpg.login.controller;

import cn.hutool.json.JSON;
import cn.srtrpg.core.util.ResultEnum;
import cn.srtrpg.core.util.ResultObject;
import cn.srtrpg.core.util.cache.CoreCache;
import cn.srtrpg.login.pojo.User;
import cn.srtrpg.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;

/**
 * @Description :
 * @Reference :
 * @Author : renminghui
 * @CreateDate : 2021-11-20 23:51
 * @Modify:
 **/
@RestController
@RequestMapping(CoreCache.BASE_URL+"/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    /**
     *  注册账号
     * @Author rmh
     * @Date 0:08 2021/11/21
     * @Param  * @param null
     * @return
     **/
    @PostMapping("/register")
    public ResultObject insertUserRegister(@RequestBody User user){
        return loginService.insertUserRegister(user);
    }

    /**
     *  登录账号
     * @Author rmh
     * @Date 0:40 2021/11/21
     * @Param  * @param null
     * @return
     **/
    @GetMapping("/in")
    public ResultObject getLoginIn(@RequestParam String userCode, @RequestParam String userPwd, HttpSession session){
        User user = new User();
        user.setUserCode(userCode);
        user.setUserPwd(userPwd);
        ResultObject res = loginService.getLoginIn(user);
        return res;
    }

    /**
     *  检查登录
     * @Author rmh
     * @Date 16:38 2021/11/21
     * @Param  * @param null
     * @return
     **/
    @GetMapping("/check")
    public ResultObject getCheckLogin(String token){
        return loginService.getCheckLogin(token);
    }

}
