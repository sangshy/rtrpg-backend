package cn.srtrpg.login.mapper;

import cn.srtrpg.login.pojo.User;
import tk.mybatis.mapper.common.BaseMapper;

public interface UserMapper extends BaseMapper<User> {

    User getUserByCode(User user);

    User getUserByLogin(User user);
}