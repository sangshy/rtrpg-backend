package cn.srtrpg.login.service;

import cn.srtrpg.core.util.ResultObject;
import cn.srtrpg.login.pojo.User;

/**
 * @Description :
 * @Reference :
 * @Author : renminghui
 * @CreateDate : 2021-11-21 0:17
 * @Modify:
 **/
public interface LoginService {
    /**
     *  注册账号
     * @Author rmh
     * @Date 0:19 2021/11/21
     * @Param  * @param user
     * @return cn.srtrpg.core.util.ResultObject
     **/
    ResultObject insertUserRegister(User user);

    /**
     *  用户登录
     * @Author rmh
     * @Date 0:45 2021/11/21
     * @Param  * @param user
     * @return cn.srtrpg.login.pojo.User
     **/
    ResultObject getLoginIn(User user);

    /**
     *  检查并返回登录信息
     * @Author rmh
     * @Date 14:23 2021/12/3
     * @Param  * @param token
     * @return cn.srtrpg.core.util.ResultObject
     **/
    ResultObject getCheckLogin(String token);
}
