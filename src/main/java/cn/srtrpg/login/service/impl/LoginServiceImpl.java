package cn.srtrpg.login.service.impl;

import cn.srtrpg.core.util.RedisCache;
import cn.srtrpg.core.util.ResultEnum;
import cn.srtrpg.core.util.ResultObject;
import cn.srtrpg.core.util.cache.CoreCache;
import cn.srtrpg.login.mapper.UserMapper;
import cn.srtrpg.login.pojo.User;
import cn.srtrpg.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @Description :
 * @Reference :
 * @Author : renminghui
 * @CreateDate : 2021-11-21 0:18
 * @Modify:
 **/
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisCache redisCache;

    @Override
    public ResultObject insertUserRegister(User user) {
        if (null == user || StringUtils.isEmpty(user.getUserCode()) || StringUtils.isEmpty(user.getUserName()) || StringUtils.isEmpty(user.getUserPwd())) {
            return new ResultObject(ResultEnum.RESULT.ERROR.getValue(),"有的没填");
        }
        User userRes = userMapper.getUserByCode(user);
        if (null != userRes) {
            return new ResultObject(ResultEnum.RESULT.ERROR.getValue(),"用户名相同，请重新输入");
        }
        int res = userMapper.insertSelective(user);
        if (res == 1) {
            return new ResultObject(ResultEnum.RESULT.SUCCESS.getValue(),"注册成功",user);
        }
        return new ResultObject(ResultEnum.RESULT.ERROR.getValue(),"注册失败");
    }

    @Override
    public ResultObject getLoginIn(User user) {
        User userRes = userMapper.getUserByCode(user);
        if (null == userRes) {
            return new ResultObject(ResultEnum.RESULT.ERROR.getValue(),"用户名不存在");
        }
        User userRes1 = userMapper.getUserByLogin(user);
        if (null == userRes1) {
            return new ResultObject(ResultEnum.RESULT.ERROR.getValue(),"用户名或密码错误");
        }
        redisCache.setObj(CoreCache.REDIS_LOGIN_URL+userRes1.getUserCode(),userRes1,30*60L);
        return new ResultObject(ResultEnum.RESULT.SUCCESS.getValue(),"登录成功",userRes1.getUserCode());
    }

    @Override
    public ResultObject getCheckLogin(String token) {
        if (StringUtils.isEmpty(token)) {
            return new ResultObject(ResultEnum.RESULT.ERROR.getValue(),"请登录");
        }
        User user = redisCache.getObj(CoreCache.REDIS_LOGIN_URL+token,User.class);
        if (null == user) {
            return new ResultObject(ResultEnum.RESULT.ERROR.getValue(),"登录过期");
        }
        return new ResultObject(ResultEnum.RESULT.SUCCESS.getValue(),"登录成功",user);
    }
}
