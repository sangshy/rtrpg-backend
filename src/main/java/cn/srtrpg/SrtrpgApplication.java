package cn.srtrpg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan(basePackages = "cn.srtrpg.*.mapper")
public class SrtrpgApplication {

    public static void main(String[] args) {
        SpringApplication.run(SrtrpgApplication.class, args);
    }

}
