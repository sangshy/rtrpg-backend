package cn.srtrpg.core.task;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author rmh
 * @version 1.0
 * @description: TODO
 * @date 2021/6/25 19:43
 */
@Configuration
@EnableScheduling
public class MyTask {

    private Logger logger;

    @Scheduled(cron = "0 0/1 * * * ?")
    public void task() throws IOException {

    }

}
