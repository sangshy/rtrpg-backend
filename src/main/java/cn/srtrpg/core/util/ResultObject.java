package cn.srtrpg.core.util;


import java.io.Serializable;

/**
 * @author rmh
 * @version 1.0
 * @description: TODO
 * @date 2021/6/16 17:43
 */
public class ResultObject<T> implements Serializable {

    private static final long serialVersionUID = 42L;

    public ResultObject(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        Data = data;
    }

    public ResultObject(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultObject(String msg) {

        this.msg = msg;
    }

    public ResultObject(T data) {
        this(ResultEnum.RESULT.SUCCESS.getValue(),ResultEnum.RESULT.SUCCESS.getDescription());
        Data = data;
    }
    /**
     *  无参 用于新增 修改 删除
     * @author rmh
     * @date 2021/6/17 10:01
     * @return null
     */
    public ResultObject() {
        this(ResultEnum.RESULT.SUCCESS.getValue(),ResultEnum.RESULT.SUCCESS.getDescription());
    }

    /**
     *  前端其实是有两套数据  一套带有返回数据类型 另一套是没有携带返回值类型
     *
     *  带返回值类型的用于查询
     *  不带返回值类型的 用于 新增 修改 删除  只需要判断 到底修成 或者 新增 删除 成功没有
     * @author rmh
     * @date 2021/6/17 9:41
     * @param null
     * @return null
     */

    private Integer code;//返回code 码

    private String msg; //返回 提示信息

    private T Data; //携带数据

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return Data;
    }

    public void setData(T data) {
        Data = data;
    }
}
