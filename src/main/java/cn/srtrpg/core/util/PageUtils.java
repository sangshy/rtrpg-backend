package cn.srtrpg.core.util;

import java.io.Serializable;
import java.util.List;

public class PageUtils<T> implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int pageNo = 2;
	private int pageSize = 5;
	private int pageStart = 0;
	private int pageCount;
	private int pageTotalCount;
	private List<T> dataList;
	
	public int getPageNo() {
		return pageNo;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
		this.pageStart = (pageNo - 1)*this.pageSize;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageCount() {
		return pageCount;
	}
	public void setPageCount(int pageCount) {
		if(pageCount % pageSize == 0){
			pageTotalCount = pageCount / pageSize;
		}else{
			pageTotalCount = pageCount / pageSize + 1;
		}
		this.pageCount = pageCount;
	}
	public int getPageTotalCount() {
		return pageTotalCount;
	}
	public void setPageTotalCount(int pageTotalCount) {
		this.pageTotalCount = pageTotalCount;
	}
	public List<T> getDataList() {
		return dataList;
	}
	public void setDataList(List<T> dataList) {
		this.dataList = dataList;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	/**
	 * @return the pageStart
	 */
	public int getPageStart() {
		return pageStart;
	}
	/**
	 * @param pageStart the pageStart to set
	 */
	public void setPageStart(int pageStart) {
		this.pageStart = pageStart;
	}

}
