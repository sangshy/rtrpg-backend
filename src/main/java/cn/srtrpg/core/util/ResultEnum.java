package cn.srtrpg.core.util;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @Description :
 * @Reference :
 * @Author : renminghui
 * @CreateDate : 2021-09-13 17:59
 * @Modify:
 **/
public class ResultEnum {

    /**
     * 返回值
     */
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum RESULT {
        SUCCESS(000000,"执行成功！"),
        ERROR(999999,"失败，请稍后再试！"),
        GET_ERROR(999998,"查询失败，请稍后再试！"),
        SAVE_ERROR(999997,"新增失败，请稍后再试！"),
        EDIT_ERROR(999996,"修改失败，请稍后再试！"),
        DEL_ERROR(999995,"删除失败，请稍后再试！"),
        XXXX_XX(200001,"没有新增权限"),
        USER_CODE_NENTITY(300001,"用户账户已存在"),
        USER_CODE_NOENTITY(300002,"用户账户不存在"),
        REGISTER_EMPTY(310001,"注册项有空项"),
        REGISTER_EXTIST(320001,"账号已存在，无法注册"),
        REGISTER_AUTH_EXTIST(320002,"验证码无效或者错误！"),
        REGISTER_ERROR(310009,"注册失败"),
        LOGIN_ERROR(320009,"用户名或密码错误，请重试"),
        ORDER_ERROR(400009,"验签失败或者网络不通畅，请重试");

        private int value;
        private String description;

        RESULT(int value, String description) {
            this.value = value;
            this.description = description;
        }

        public int getValue() {
            return value;
        }

        public String getDescription() {
            return description;
        }

        public static String valueOfDescription(int val) {
            String tmp = null;
            for (int i = 0; i < values().length; i++) {
                if (values()[i].value == val) {
                    tmp = values()[i].description;
                }
            }
            return tmp;
        }

        public static int descriptionOfValue(String description) {
            int tmp = 0;
            for (int i = 0; i < values().length; i++) {
                if (values()[i].description.equals(description)) {
                    tmp = values()[i].value;
                }
            }
            return tmp;
        }
    }


}
