package cn.srtrpg.core.util;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RedisCache {

    /**
     *  封装String类型Set方法
     * @author rmh
     * @date 2021/6/15 11:27
     * @param key
     * @param keyFiled
     * @param object
     */
    void set(String key,String keyFiled,Object object);



    <K,V>void putAll(String key,Map<K,V> map);

    <T> Map hgetpushAll(String key,Class<T> clazz);


    /**
     * 根据规则查询所有符合的Key
     *
     * @param pattern
     * @return
     */
    public Set<String> keys(String pattern);

    /**
     * 返回当前Redis库的 key的数量
     *
     * @return
     */
    public Long dbSize();

    /**
     * 清空当前Redis库
     *
     * @return
     */
    public Boolean flushDB();

    /**
     * 根据key删除数据
     *
     * @param key
     */
    public void delete(String key);

    /**
     * 根据keys集合批量删除数据
     *
     * @param keys
     */
    public void delete(Collection<String> keys);

    /**
     * 获取分布式锁，设置获取锁超时时间，超过时限获取锁失败
     * @param lockKey 锁的Key
     * @param lockValue 存入锁的值
     * @param lockSeconds 获取锁成功后设置的过期时间（单位：秒）
     * @param timeOutSeconds 获取锁的超时时间
     * @return 是否获取成功
     */
    public Boolean getWaitLock(String lockKey, String lockValue, Long lockSeconds, Long timeOutSeconds);

    /**
     * 尝试加入分布式锁
     * @param lockKey 锁的Key
     * @param lockValue 存入锁的值
     * @param lockSeconds 获取锁成功后设置的过期时间（单位：秒）
     * @return 是否加锁成功
     */
    public Boolean tryLock(String lockKey, String lockValue, Long lockSeconds);

    /**
     * 尝试释放分布式锁
     * @param lockKey 锁的Key
     * @param lockValue 存入锁的值
     * @return 是否释放成功
     */
    public Boolean unLock(String lockKey, String lockValue);

    /**
     * 批量加锁
     * @param lockKeySet
     * @param lockValue
     * @param lockSeconds
     * @return
     */
    public Boolean tryLockBatch(Set<String> lockKeySet, String lockValue, long lockSeconds);

    /**
     * 批量解锁
     *
     * @param lockKeySet
     * @param lockValue
     * @return
     */
    public Boolean unlockBatch(Set<String> lockKeySet, String lockValue);

    /**
     * 以秒为单位，返回给定 key 的剩余生存时间(TTL, time to live)。
     *
     * @param key
     * @return
     */
    public Long ttl(String key);

    /**
     * 根据key设置值
     *
     * @param key
     * @param value
     */
    public void set(String key, Object value);

    /**
     * 根据key设置值并设置超时时间（秒）
     *
     * @param key
     * @param value
     * @param timeOutSeconds
     */
    public void set(String key, Object value, Long timeOutSeconds);

    /**
     * 根据key获取数据
     *
     * @param key
     * @return
     */
    public <T> T get(String key);

    /**
     * 根据key设置对象到redis中
     *
     * @param key
     * @param value
     */
    public void setObj(String key, Object value);

    /**
     * 根据key设置对象到redis中并设置超时时间（秒）
     *
     * @param key
     * @param value
     * @param timeOutSeconds
     */
    public void setObj(String key, Object value, Long timeOutSeconds);

    /**
     * 根据key获取数据
     *
     * @param key
     * @return
     */
    public <T> T getObj(String key);

    /**
     * 根据key及返回对象引用类型获取数据
     *
     * @param key
     * @param type
     * @return
     */
    public <T> T getObj(String key, TypeReference<T> type);

    /**
     * 根据key及返回类类型获取数据
     *
     * @param key
     * @param clazz
     * @return
     */
    public <T> T getObj(String key, Class<T> clazz);

    /**
     * 根据key添加List到redis中，key不存在则新增
     *
     * @param key
     * @param list
     */
    public <T> void addList(String key, List<T> list);

    /**
     * 根据key增量添加List，key不存在则新增并设置超时时间（秒）
     *
     * @param key
     * @param list
     * @param timeOutSeconds
     */
    public <T> void addList(String key, List<T> list, Long timeOutSeconds);

    /**
     * 根据key设置List，key存在则覆盖原有数据
     *
     * @param key
     * @param list
     */
    public <T> void setList(String key, List<T> list);

    /**
     * 根据key设置List，key存在则覆盖原有数据并设置超时时间（秒）
     *
     * @param key
     * @param list
     * @param timeOutSeconds
     */
    public <T> void setList(String key, List<T> list, Long timeOutSeconds);

    /**
     * 根据key获取List
     *
     * @param key
     * @return
     */
    public <T> List<T> getList(String key);

    /**
     * 根据key及返回类类型获取List
     *
     * @param key
     * @param clazz
     * @return
     */
    public <T> List<T> getList(String key,Class<T> clazz);

    /**
     * 根据key及返回对象引用类型获取数据
     *
     * @param key
     * @param type
     * @return
     */
    public <T> List<T> getList(String key, TypeReference<T> type);

    /**
     * 根据Redis的key，Map的HashKey设置Map数据
     *
     * @param key
     * @param hashKey
     * @param value
     */
    public <T> void putHash(String key, String hashKey, T value);

    /**
     * 根据Redis的key，Map的HashKey设置Map数据并设置超时时间（秒）
     *
     * @param key
     * @param hashKey
     * @param value
     * @param timeOutSeconds
     */
    public <T> void putHash(String key, String hashKey, T value, Long timeOutSeconds);

    /**
     * 根据Redis的key设置Map数据
     *
     * @param key
     */
    public <K,V> void putHashAll(String key, Map<K,V> map);

    /**
     * 根据Redis的key设置Map数据并设置超时时间（秒）
     *
     * @param key
     * @param timeOutSeconds
     */
    public <K,V> void putHashAll(String key, Map<K,V> map, Long timeOutSeconds);

    /**
     * 根据Redis的key，Map的HashKey获取Map中HashKey对应的值
     *
     * @param key
     * @param hashKey
     */
    public <T> T getHash(String key, String hashKey);

    /**
     * 根据Redis的key，Map的HashKey，返回类类型获取Map中HashKey对应的值
     *
     * @param key
     * @param hashKey
     * @param clazz
     */
    public <T> T getHash(String key, String hashKey, Class<T> clazz);

    /**
     * 根据Redis的key，Map的HashKey，返回值引用类型获取Map中HashKey对应的值
     *
     * @param key
     * @param hashKey
     * @param type
     * @return
     */
    public <T> T getHash(String key, String hashKey, TypeReference<T> type);

    @SuppressWarnings("rawtypes")
    public <T> List<T> getMHash(String key, List hashKeys);

    public <T> List<T> getMHash(String key, List<String> hashKeys, Class<T> clazz);

    public <T> List<T> getMHash(String key, List<String> hashKeys, TypeReference<T> type);

    /**
     * 根据Redis的key获取整个Map数据
     *
     * @param key
     * @return
     */
    @SuppressWarnings("rawtypes")
    public Map getHashAll(String key);

    /**
     *
     *
     * @param key
     * @param clazz
     * @return
     */
    @SuppressWarnings("rawtypes")
    public <T> Map getHashAll(String key, Class<T> clazz);

    /**
     * 根据Redis的key，返回值引用类型获取整个Map数据
     *
     * @param key
     * @param type
     * @return
     */
    public <V> Map<String,V> getHashAll(String key, TypeReference<V> type);

    /**
     * 根据Redis的key，Map的hashKeys批量删除Map中hashKeys对应的值
     *
     * @param key
     * @return
     */
    public void delHash(String key, Object... hashKeys);

    /**
     * 将 key中储存的数字加上指定的增量值，key不存在则直接设置为增量值
     *
     * @param key
     * @param increment
     * @return
     */
    public Long increment(String key, int increment);

    /**
     * 设置 key 的过期时间，单位：秒
     *
     * @param key
     * @param timeOutSeconds
     */
    public void expire(String key, Long timeOutSeconds);
}
