package cn.srtrpg.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Description :
 * @Reference :
 * @Author : renminghui
 * @CreateDate : 2021-11-23 16:49
 * @Modify:
 **/
@Controller
public class PageController {
    @RequestMapping("/index")
    public String index(ModelAndView model) {
        model.setViewName("index");
        return "index";
    }
}
